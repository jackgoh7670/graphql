<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;
use App\Http\Controllers\CommentController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get("/", function () {
    return view("home");
})->name("home");



//testting
Route::get('/blogs/{id}', [PostController::class, 'list']);

Route::post('/blogs/update/{id}', [PostController::class, 'update']);

Route::delete('/blogs/delete/{id}', [PostController::class, 'delete']);







// to show list of post
Route::get("/posts/list", [PostController::class, 'index'])->name('posts.index');
// to show form for create new post
Route::get("/posts/create", [PostController::class, 'create'])->name('posts.create');
// route to store new post
Route::post("/posts/store", [PostController::class, 'store'])->name('posts.store');
// to show single post
Route::get("/posts/{id}", [PostController::class, 'show'])->name('posts.show');
// to show form to update post
Route::get("/posts/{id}/edit", [PostController::class, 'edit'])->name('posts.edit');
// route to store new post
Route::put("/posts/{id}/update", [PostController::class, 'update'])->name('posts.update');
// route to store new post
Route::delete("/posts/{id}/delete", [PostController::class, 'delete'])->name('posts.delete');

// to add new comments inside post
Route::post("/posts/{id}/addComment", [CommentController::class, 'store'])->name("comments.store");


// Login Register
