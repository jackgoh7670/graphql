@extends('layouts.master')

@section('content')
<div class="row">
        @for ($i = 0; $i < 20; $i++)
        <div class="col-md-3 p-3">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Lorem Title</h5>
                    <p class="card-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus nisi corporis vero expedita quasi corrupti sunt vel perspiciatis ullam itaque doloribus officia possimus odio voluptatem dignissimos ipsum ipsa, cumque molestiae.</p>
                    {{-- <p class="card-text">{{  substr($post->body,0,50) }}</p> --}}
                    <p>author : Khai</p>
                    <a href="#" class="btn btn-danger">Read More..</a>
                </div>
            </div>
        </div>
        @endfor

</div>
@endsection
