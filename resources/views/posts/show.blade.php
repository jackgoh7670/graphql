@extends('layouts.master')

@section('content')
<div class="row justify-content-center">
    <div class="col-md-12 mb-3">
        <div class="card">
            <div class="card-body">
                <h3>
                    {{$post->id}} 
                    <span class="float-end">
                        <a href="{{ route("posts.index") }}" class="btn btn-info">Back</a>
                    </span>
                </h3>
                <hr>
                <span class="text-danger">{{$post->title}}</span>
                <br>
                {{-- <small>
                    @foreach ($post->tags as $tag)
                        <span class="badge badge-primary">{{  $tag->name }}</span>
                    @endforeach
                </small> --}}
                <section>
                    {{$post->body}}
                </section>
            </div>
        </div>
    </div>

    <div class="col-md-12">

        <div class="card mt-3">
            <div class="card-body">
                <form action="{{route('comments.store', $post->id)}}" method="post">
                    @csrf
                    <div class="form-group">
                      <label>Comment Here</label>
                      <textarea name="comment" class="form-control" cols="30" rows="10"></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary mt-2">Add Comment</button>
                </form>
            </div>
        </div>

@foreach ($comments as $comment)
    

            <div class="card mt-3">
                <div class="card-body">

                    ALi said {{$comment->created_at->diffForHumans()}}<br>
                   {{$comment->body}}<br>
                    <hr>
                 
                </div>
            </div>
            @endforeach
    </div>
   
</div>

@endsection
