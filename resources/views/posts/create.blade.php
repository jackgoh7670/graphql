@extends('layouts.master')

@section('content')
<div class="row justify-content-center">
    <div class="col-md-12 mb-3">
        <div class="card">
            <div class="card-body">
                <form action="{{route('posts.store')}}" method="post">
                    @csrf
                    <div class="form-group">
                      <label>Title</label>
                      <input type="text" name="title" class="form-control @error('title') is-invalid @enderror" placeholder="">
                      @error('title')
                            <span class="text-danger">{{ $message }}</span>
                      @enderror
                    </div>
                    <div class="form-group">
                        <label>Body</label>
                        <textarea name="body" class="form-control @error('body') is-invalid @enderror" cols="30" rows="10"></textarea>
                        @error('body')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-primary">Create</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
