@extends('layouts.master')

@section('content')
<div class="row justify-content-center">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h4>
                    Post List
                    <span class="float-end">
                        <a class="btn btn-primary" href="{{  route("posts.create") }}">New Post</a>
                    </span>
                </h4>
                <hr>
                <table class="table">
                    <thead>
                      <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Title</th>
                        <th scope="col">Body</th>
                        <th scope="col">Status</th>
                        <th scope="col">Created At</th>
                        <th scope="col">Action</th>

                      </tr>
                    </thead>



    

                    <tbody>
                      @foreach ($posts as $post)
                      <tr>
                        <th scope="row">{{$post->id}}</th>
                      
                        <td>{{$post->title}}</td>
                        <td>{{  substr($post->body,0,10)}}</td>
                        <td>{{$post->is_active}}</td>
                        <td>{{$post->created_at}}</td>
                      
                        

                       
                        <td>
                            <a href="{{route('posts.show', $post->id)}}" class="btn btn-success">View</a>
                        </td>
                        <td>
                          <form action="/blogs/delete/{{$post->id}}" method="post">
    
    
                            @csrf
                            @method("DELETE")
        
                            <button type="submit" class="btn btn-danger">Delete</button>
        
        
                        </form>
                        </td>
                        <td>
                            <a href="{{  route("posts.edit", $post->id) }}" class="btn btn-info">Edit</a>
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                </table>
                {{ $posts->links() }}

            </div>
        </div>
    </div>
</div>
@endsection
