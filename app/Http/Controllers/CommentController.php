<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function store(Request $request,$id)
    {
     //connect comment model

     $comment = new Comment;



     //assign request data to table column

        $comment-> body =$request-> comment;

        $comment->post_id = $id;

        $comment->save();


     //return post detail page
     //return redirect()->route('posts.show',$id);

     return back();
    }
}
