<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use Illuminate\Support\Facades\Redis;

class PostController extends Controller
{

    //--------------------------------//
    public function list($id){

        $cachedBlog = Redis::get('post' . $id);

                if(isset($cachedBlog)) {
                    $blog = json_decode($cachedBlog, FALSE);

                    return response()->json([
                        'status_code' => 201,
                        'message' => 'Fetched from redis',
                        'data' => $blog,
                    ]);
                }else {
                    $blog = Post::find($id);
                    Redis::set('post' . $id, $blog);

                    return response()->json([
                        'status_code' => 201,
                        'message' => 'Fetched from database',
                        'data' => $blog,
                    ]);
                }
}


  public function update(Request $request, $id) {

    $update = Post::findOrFail($id)->update($request->all());
  
    if($update) {
  
        // Delete blog_$id from Redis
        Redis::del('post' . $id);
  
        $blog = Post::find($id);
        // Set a new key with the blog id
        Redis::set('post' . $id, $blog);
  
        return response()->json([
            'status_code' => 201,
            'message' => 'User updated',
            'data' => $blog,
        ]);
    }
 

    }

    public function delete($id) {

        Post::findOrFail($id)->delete();
        Redis::del('post' . $id);
      
        return response()->json([
            'status_code' => 201,
            'message' => 'Blog deleted'
        ]);
      }


    //--------------------------------//

    public function index()
    {

        //$posts = Post::get();
        $posts = Post::latest()->paginate(100);



        return view("posts.index", compact('posts',));
    }


    public function create()
    {
        return view("posts.create");
    }

    public function store(Request $request)
    {

       $post = new Post;
       $post->title = $request->title;

       

       $post->body = $request->body;

       $post->is_active = 1;

       $post->save();

       return redirect()->route('posts.index');



    }

    public function show($id)
    {
        //connect to the model
        $post = Post::find($id);


        $comments = $post ->comments;

        return view("posts.show", compact('post','comments'));
    }

    public function edit()
    {
        return view("posts.edit");
    }

    // public function update()
    // {
    // }

    // public function delete()
    // {
    // }
}
