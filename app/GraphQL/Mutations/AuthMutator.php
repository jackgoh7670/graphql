<?php

namespace App\GraphQL\Mutations;

use App\Models\User;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use GraphQL\Type\Definition\ResolveInfo;
use Illuminate\Support\Facades\Hash;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use App\Models\peronalAccessTokens;




class AuthMutator
{
    /**
     * @param  null  $_
     * @param  array<string, mixed>  $args
     */
    public function __invoke($_, array $args)
    {
        // TODO implement the resolver
    }


    public function resolve($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $credentials = Arr::only($args, ['email', 'password']);

        if (Auth::once($credentials)) {
            $token = Str::random(60);

            $user = auth()->user();
            $user->createToken('token-name', ['user'])->plainTextToken;
            $user->save();

            return $token;
        }

        return null;
    }


    


    // public function resolve($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    // {
    //     $credentials = Arr::only($args, ['email', 'password']);

    //     $user = User::where('email', $args['email'])->first();

    //     if (! $user || ! Hash::check($args['password'], $user->password)) {
    //         return 'Invalid email or password';
    //     }

    //     // Auth::login($user);
    //     $token = $user->createToken('auth_token')->plainTextToken;

    //     return ([
    //         'token' => $token,
    //         'user' => $user
    //     ]);
    // }



    // public function login($root, array $args)
    // {
    //     // $credentials = Arr::only($args, ['email', 'password']);

    //     $user = User::where('email', $args['email'])->first();

    //     if (! $user || ! Hash::check($args['password'], $user->password)) {
    //         return 'Invalid email or password';
    //     }

    //     // Auth::login($user);
    //     $token = $user->createToken('auth_token')->plainTextToken;

    //     return ([
    //         'token' => $token,
    //         'user' => $user
    //     ]);
    // }
// public function login()
// {

// }

}
